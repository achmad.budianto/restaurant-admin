import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationComponent } from './reservation.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';



@NgModule({
  declarations: [ReservationComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule,
  ],
})
export class ReservationModule { }
