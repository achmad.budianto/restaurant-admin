import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ReservationService } from '../../@core/services/reservation/reservation.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss'],
})
export class ReservationComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'id',
      },
      name: {
        title: 'Name',
      },
      email: {
        title: 'Email',
      },
      phone: {
        title: 'Phone',
      },
      persons: {
        title: 'Persons',
      },
      date: {
        title: 'Date',
      },
      time: {
        title: 'Time',
      },
      message: {
        title: 'Message',
      },
    },
    hideSubHeader: true, // hide filter row
    actions: {
      columnTitle: 'Actions',
      position: 'right',
      custom: [
        {
          name: 'APPROVE',
          title: `<i class="nb-checkmark bigger-icon" title="Approve"></i>`,
        },
        {
          name: 'APPROVE',
          title: `<i class="nb-close bigger-icon" title="Reject"></i>`,
        },
      ],
      add: false,
      edit: false,
      delete: false,
    }, // hide action column
  };

  source: LocalDataSource; // add a property to the component
  role: string;

  constructor(
    public datepipe: DatePipe,
    private reservationService: ReservationService,
  ) { }

  ngOnInit(): void {
    this.generateTable();
  }

  generateTable() {
    const datas = [];

    this.reservationService.getReservation().subscribe(results => {

      results.forEach(item => {
        datas.push({
          id: item.id,
          name: item.name,
          email: item.email,
          phone: item.phone,
          persons: item.persons,
          date: this.datepipe.transform(new Date(item.date), 'dd-MM-yyyy'),
          time: item.time,
          message: item.message,
        });
      });
      this.source = new LocalDataSource(datas);
    });
  }

}
