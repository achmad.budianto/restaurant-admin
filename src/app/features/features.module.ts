import { NgModule } from '@angular/core';
import { FeaturesComponent } from './features.component';
import { FeaturesRoutingModule } from './faetures-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { NbMenuModule } from '@nebular/theme';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  imports: [
    FeaturesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
  ],
  declarations: [
    FeaturesComponent,
  ],
})
export class FeaturesModule { }
