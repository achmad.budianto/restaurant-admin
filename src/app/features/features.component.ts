import { Component } from '@angular/core';

import { MENU_ITEMS } from './faetures-menu';

@Component({
  selector: 'ngx-features',
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
  styleUrls: ['./features.component.scss'],
})
export class FeaturesComponent {

  menu = MENU_ITEMS;

}
