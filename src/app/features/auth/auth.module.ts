import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { NbAlertModule, NbInputModule, NbButtonModule, NbCardModule, NbLayoutModule } from '@nebular/theme';
import { AuthComponent } from './auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [LoginComponent, AuthComponent, LogoutComponent],
  imports: [
    CommonModule,
    NbAlertModule,
    NbButtonModule,
    NbInputModule,
    NbCardModule,
    FormsModule,
    NbLayoutModule,
    ReactiveFormsModule,
    AuthRoutingModule,
  ],
})
export class AuthModule { }
