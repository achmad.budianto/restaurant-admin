import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthenticationService } from '../../../@core/services/authentication/authentication.service';
import { LocalStorageEnum } from '../../../@core/enum/local-storage.enum';
import { LoginDto } from '../../../@core/dto/login.dto';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  signinFormGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.createFormControl();
  }

  /* Fungsi untuk membuat nama field pada form */
  createFormControl() {
    this.signinFormGroup = this.fb.group({
      email: ['', [
        Validators.required,
      ]],
      password: ['', [
        Validators.required,
      ]],
    });
  }

  onSubmit() {
    const login: LoginDto = this.signinFormGroup.value;
    this.authenticationService.doLogin(login).subscribe(
    result => {
      // Handle result
      if (result.ok) {
        localStorage.setItem(LocalStorageEnum.TOKEN_KEY, result.data.token);
        this.router.navigate(['/pages/dashboard']);
      } else {
        Swal.fire(
          'Sorry!',
          result.message,
          'error',
        );
      }
    },
    error => {
      Swal.fire(
        'Sorry!',
        error.error.message,
        'error',
      );
      },
    );
  }
}
