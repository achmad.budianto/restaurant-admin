import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../@core/services/authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-logout',
  template: '',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.authenticationService.doLogout();
    this.router.navigateByUrl('/auth/login');
  }

}
