import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../@core/services/authentication/authentication.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  user: any;
  role: string;

  constructor(
    private authService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.user = this.authService.getActiveUser();
    this.role = this.user.role;
  }

}
