import { NbMenuItem } from '@nebular/theme';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocalStorageEnum } from '../@core/enum/local-storage.enum';

let activeRole = '-';

const helper = new JwtHelperService();
const token = localStorage.getItem(LocalStorageEnum.TOKEN_KEY);

if (token) {
  const decodedToken = helper.decodeToken(token);
  activeRole = decodedToken.role;
}

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Beranda',
    icon: 'home-outline',
    link: '/beranda',
    home: true,
  },

  {
    title: 'Transactions',
    group: true,
  },
  {
    title: 'Reservation',
    icon: 'file-outline',
    link: '/reservation',
  },
];
