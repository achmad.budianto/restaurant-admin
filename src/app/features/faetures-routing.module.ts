import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FeaturesComponent } from './features.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from '../@core/services/authentication/authguard.service';
import { ReservationComponent } from './reservation/reservation.component';

const routes: Routes = [{
  path: '',
  component: FeaturesComponent,
  children: [
    {
      path: 'beranda',
      component: DashboardComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: 'reservation',
      component: ReservationComponent,
      canActivate: [AuthGuardService],
    },
    {
      path: '',
      redirectTo: 'auth/login',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeaturesRoutingModule {
}
