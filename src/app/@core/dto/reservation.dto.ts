export class ReservationsRO {
  id: number;
  name: string;
  email: string;
  phone: string;
  persons: number;
  date: Date;
  time: string;
  message: string;
}
