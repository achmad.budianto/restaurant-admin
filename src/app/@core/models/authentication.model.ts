import { BaseResponse } from './base-response.model';

export class Token {
  token: string;
  message: string;
  status: string;

  constructor() {}
}

export class UserData {
  email: string;
  token: string;
  username: string;
  bio: string;
  image: string;
}

export class LoginRO extends BaseResponse {
  data: UserData;
}

export class ForgotPassword {
  email: any;
  type: string;
  status: number;
  message: string;
}

export class ResetPassword {
  key: any;
  newPassword: any;
  status: number;
  message: string;
}
