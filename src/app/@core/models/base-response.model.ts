export class BaseResponse {
  ok: boolean;
  message: string;
}
