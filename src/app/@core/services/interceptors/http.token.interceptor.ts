import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import { AuthenticationService } from '../authentication/authentication.service';
import { LocalStorageEnum } from '../../enum/local-storage.enum';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  constructor(private inj: Injector, private routes: Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const auth = this.inj.get(AuthenticationService);
    const token = auth.getToken();

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
    }

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          if (token) {
            localStorage.removeItem(LocalStorageEnum.TOKEN_KEY);
            this.routes.navigateByUrl('/auth/login');
          }
          this.routes.navigateByUrl('/auth/login');
        } else if (err.status === 403) {
          if (token) {
            localStorage.removeItem(LocalStorageEnum.TOKEN_KEY);
            this.routes.navigateByUrl('/auth/login');
          }
          this.routes.navigateByUrl('/auth/login');
        } else if (err.status === 404) {
        } else if (err.status === 400) {
          // swal('Oops!...something wrong...')
          // .then((result) => {
          //   //this.routes.navigateByUrl('/404');
          // });
        } else if (err.status === 500) {
          // swal('belisada.co.id', 'Oops!...something wrong... 500', 'error');
          // .then((result) => {
          //  this.routes.navigateByUrl('/maintenance');
         // });
        } else {
          // swal('Oops!...something wrong...')
          // .then((result) => {
          //  this.routes.navigateByUrl('/maintenance');
         // });
        }
      }
    });

  }
}
