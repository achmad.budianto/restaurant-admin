import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { LocalStorageEnum } from '../../enum/local-storage.enum';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {

  constructor(public auth: AuthenticationService, public router: Router) {}

  canActivate(): boolean {

    if (!this.auth.isAuthenticated()) {
      localStorage.removeItem(LocalStorageEnum.TOKEN_KEY);
      this.router.navigate(['/auth/login']);
      return false;
    }
    return true;
  }
}
