import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { HttpHeaders } from '@angular/common/http/src/headers';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Configuration } from '../../config/configuration';

import 'rxjs/add/operator/map';
import { LoginRO } from '../../models/authentication.model';
import { LocalStorageEnum } from '../../enum/local-storage.enum';
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  constructor(
    private configuration: Configuration,
    private http: HttpClient,
  ) { }

  public getActiveUser() {
    const helper = new JwtHelperService();
    const token = localStorage.getItem(LocalStorageEnum.TOKEN_KEY);

    const decodedToken = helper.decodeToken(token);
    return decodedToken;
  }

  public isAuthenticated(): boolean {
    const helper = new JwtHelperService();
    const token = localStorage.getItem('token');
    // Check whether the token is expired and return
    // true or false
    return !helper.isTokenExpired(token);
  }

  /*
  param:
  Used by: app.module.ts
  Description: Fungsi ini mengambil token yang tersimpan pada local storage.
  */
  getToken() {
    const token = localStorage.getItem('token');
    if (token) {
      return token;
    }
  }

  doLogin(data) {
    return this.http.post(this.configuration.apiURL + '/users/login', data)
      .map(resp => resp as LoginRO);
  }

  doLogout() {
    localStorage.removeItem(LocalStorageEnum.TOKEN_KEY);
  }
}
