import { Injectable } from '@angular/core';
import { Configuration } from '../../config/configuration';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ReservationsRO } from '../../dto/reservation.dto';

@Injectable({
  providedIn: 'root',
})
export class ReservationService {

  constructor(
    private configuration: Configuration,
    private http: HttpClient,
  ) { }

  getReservation() {
    return this.http.get(this.configuration.apiURL + '/reservations')
      .pipe(map(resp => resp as ReservationsRO[]));
  }
}
